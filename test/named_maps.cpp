#include "named_map.h"
#include "named_unordered_map.h"
#include "gtest/gtest.h"
#include <iostream>
#include <vector>

using FooBarMap = NamedMap(int, foo, int, bar);
TEST(NamedMap, InsertAndAccess) {

    std::vector<int> map_entries{1, 2, 3, 4, 5, 6, 7, 8, 9};
    FooBarMap        test_map;
    for(auto i = 0; i < map_entries.size(); ++i) {
        test_map[i] = map_entries[i];
    }

    ASSERT_EQ(map_entries.size(), test_map.size());
    for(auto i = 0; i < map_entries.size(); ++i) {
        EXPECT_EQ(test_map[i], map_entries[i]);
    }

    for(const auto &entry : test_map) {
        std::cout << "entry.foo: " << entry.foo << ", entry.bar: " << entry.bar << std::endl;
    }
}

using UnorderedFooBarMap = NamedUnorderedMap(int, foo, int, bar);
TEST(NamedUnorderedMap, InsertAndAccess) {

    std::vector<int>   map_entries{1, 2, 3, 4, 5, 6, 7, 8, 9};
    UnorderedFooBarMap test_map;
    for(auto i = 0; i < map_entries.size(); ++i) {
        test_map[i] = map_entries[i];
    }

    ASSERT_EQ(map_entries.size(), test_map.size());
    for(auto i = 0; i < map_entries.size(); ++i) {
        EXPECT_EQ(test_map[i], map_entries[i]);
    }

    for(const auto &entry : test_map) {
        std::cout << "entry.foo: " << entry.foo << ", entry.bar: " << entry.bar << std::endl;
    }
}
