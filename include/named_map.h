#include <map>
#include <utility>

#define NamedMap(KEY_TYPE, firstname, VALUE_TYPE, secondname)                                                              \
    class __NAMED_MAP_IMPL__##firstname##secondname##KEY_TYPE##VALUE_TYPE                                                  \
      : public std::map<KEY_TYPE, VALUE_TYPE> {                                                                            \
                                                                                                                           \
        template <class T1, class T2>                                                                                      \
        class NamedPair : public std::pair<T1, T2> {                                                                       \
        public:                                                                                                            \
            constexpr NamedPair() : std::pair<T1, T2>(), firstname(this->first),                                           \
                                    secondname(this->second){};                                                            \
            constexpr NamedPair(const T1 &x, const T2 &y) : std::pair<T1, T2>(x, y), firstname(this->first),               \
                                                            secondname(this->second) {}                                    \
            NamedPair(const std::pair<T1, T2> &p) : std::pair<T1, T2>(p), firstname(this->first),                          \
                                                    secondname(this->second) {}                                            \
            NamedPair(std::pair<T1, T2> &&p) : std::pair<T1, T2>(std::forward(p)), firstname(this->first),                 \
                                               secondname(this->second) {}                                                 \
                                                                                                                           \
            template <class U1, class U2>                                                                                  \
            constexpr NamedPair(U1 &&x, U2 &&y) : std::pair<T1, T2>(std::forward(x), std::forward(y)),                     \
                                                  firstname(this->first), secondname(this->second) {}                      \
                                                                                                                           \
            template <class U1, class U2>                                                                                  \
            constexpr NamedPair(const std::pair<U1, U2> &p) : std::pair<T1, T2>(p), firstname(this->first),                \
                                                              secondname(this->second) {}                                  \
                                                                                                                           \
            template <class U1, class U2>                                                                                  \
            constexpr NamedPair(const std::pair<U1, U2> &&p) : std::pair<T1, T2>(std::forward(p)), firstname(this->first), \
                                                               secondname(this->second) {}                                 \
                                                                                                                           \
            template <class... Args1, class... Args2>                                                                      \
            NamedPair(std::piecewise_construct_t, std::tuple<Args1...> first_args,                                         \
                std::tuple<Args2...> second_args) :                                                                        \
              std::pair<T1, T2>(std::piecewise_construct, first_args,                                                      \
                  second_args),                                                                                            \
              firstname(this->first), secondname(this->second) {}                                                          \
                                                                                                                           \
            T1 &firstname;                                                                                                 \
            T2 &secondname;                                                                                                \
                                                                                                                           \
        private:                                                                                                           \
            using std::pair<T1, T2>::first;                                                                                \
            using std::pair<T1, T2>::second;                                                                               \
        };                                                                                                                 \
                                                                                                                           \
        class __named_iterator__                                                                                           \
          : public std::map<KEY_TYPE, VALUE_TYPE>::iterator {                                                              \
        public:                                                                                                            \
            __named_iterator__(                                                                                            \
                const typename std::map<KEY_TYPE, VALUE_TYPE>::iterator &other) :                                          \
              std::map<KEY_TYPE, VALUE_TYPE>::iterator(other) {}                                                           \
                                                                                                                           \
            NamedPair<KEY_TYPE, VALUE_TYPE> operator*() {                                                                  \
                return NamedPair<KEY_TYPE, VALUE_TYPE>(                                                                    \
                    std::map<KEY_TYPE, VALUE_TYPE>::iterator::operator*());                                                \
            }                                                                                                              \
        };                                                                                                                 \
                                                                                                                           \
        class __const_named_iterator__                                                                                     \
          : public std::map<KEY_TYPE, VALUE_TYPE>::const_iterator {                                                        \
        public:                                                                                                            \
            __const_named_iterator__(                                                                                      \
                const typename std::map<KEY_TYPE, VALUE_TYPE>::const_iterator                                              \
                    &other) :                                                                                              \
              std::map<KEY_TYPE, VALUE_TYPE>::const_iterator(other) {}                                                     \
                                                                                                                           \
            NamedPair<KEY_TYPE, VALUE_TYPE> operator*() {                                                                  \
                return NamedPair<KEY_TYPE, VALUE_TYPE>(                                                                    \
                    std::map<KEY_TYPE, VALUE_TYPE>::const_iterator::operator*());                                          \
            }                                                                                                              \
        };                                                                                                                 \
                                                                                                                           \
    public:                                                                                                                \
        __NAMED_MAP_IMPL__##firstname##secondname##KEY_TYPE##VALUE_TYPE() : std::map<KEY_TYPE, VALUE_TYPE>() {}            \
                                                                                                                           \
        __NAMED_MAP_IMPL__##firstname##secondname##KEY_TYPE##VALUE_TYPE(                                                   \
            const std::map<KEY_TYPE, VALUE_TYPE> &other) :                                                                 \
          std::map<KEY_TYPE, VALUE_TYPE>(other) {}                                                                         \
                                                                                                                           \
        __NAMED_MAP_IMPL__##firstname##secondname##KEY_TYPE##VALUE_TYPE(                                                   \
            std::map<KEY_TYPE, VALUE_TYPE> &&other) :                                                                      \
          std::map<KEY_TYPE, VALUE_TYPE>(                                                                                  \
              std::forward<std::map<KEY_TYPE, VALUE_TYPE>>(other)) {}                                                      \
                                                                                                                           \
        template <class value_type>                                                                                        \
        __NAMED_MAP_IMPL__##firstname##secondname##KEY_TYPE##VALUE_TYPE(                                                   \
            std::initializer_list<value_type> init) :                                                                      \
          std::map<KEY_TYPE, VALUE_TYPE>(init) {}                                                                          \
                                                                                                                           \
        template <class InputIt>                                                                                           \
        __NAMED_MAP_IMPL__##firstname##secondname##KEY_TYPE##VALUE_TYPE(                                                   \
            InputIt first, InputIt last) :                                                                                 \
          std::map<KEY_TYPE, VALUE_TYPE>(first, last) {}                                                                   \
                                                                                                                           \
        __named_iterator__ begin() noexcept {                                                                              \
            return __named_iterator__(std::map<KEY_TYPE, VALUE_TYPE>::begin());                                            \
        }                                                                                                                  \
        __named_iterator__ end() noexcept {                                                                                \
            return __named_iterator__(std::map<KEY_TYPE, VALUE_TYPE>::end());                                              \
        }                                                                                                                  \
                                                                                                                           \
        __const_named_iterator__ begin() const noexcept {                                                                  \
            return __const_named_iterator__(                                                                               \
                std::map<KEY_TYPE, VALUE_TYPE>::begin());                                                                  \
        }                                                                                                                  \
        __const_named_iterator__ end() const noexcept {                                                                    \
            return __const_named_iterator__(std::map<KEY_TYPE, VALUE_TYPE>::end());                                        \
        }                                                                                                                  \
                                                                                                                           \
        __const_named_iterator__ cbegin() const noexcept {                                                                 \
            return __const_named_iterator__(                                                                               \
                std::map<KEY_TYPE, VALUE_TYPE>::cbegin());                                                                 \
        }                                                                                                                  \
        __const_named_iterator__ cend() const noexcept {                                                                   \
            return __const_named_iterator__(std::map<KEY_TYPE, VALUE_TYPE>::cend());                                       \
        }                                                                                                                  \
    }